<?php

$host = 'localhost';
$user = 'root';
$pass = '';
$db   = 'questions';

$dsn  = 'mysql:dbname=' . $db . ';host=' . $host . ';charset=utf8';

$settings = [
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION
];

try {
    $dbm = new PDO($dsn, $user, $pass, $settings);
} catch (PDOException $exception) {
    echo 'Failed to connect to db: ' . $exception->getMessage();
    die;
}
