# Questionnaire
## Getting started

1. Install MariaDB or MySQL
2. Install PHP along with the JSON and MySQL extension (the MySQL extension works for MariaDB, too)
3. Start MariaDB's/MySQL's service
4. Enter your database's shell via `mysql -u root -p`, and then entering nothing as password
5. Inside the shell, run `CREATE DATABASE questions;`, followed by `SOURCE migrate.sql;`. This sets up the database for storing data.
6. To add your own questions, edit `questions.json`.
7. Assuming everything went as expected, you should now be able to run this via `php -S 127.0.0.1:8080`, and then open `http://127.0.0.1:8080` in your browser of choice to see it. It should also work in Apache and NGINX.
## Note

I had explicit consent from my client to release these files as open source. 
