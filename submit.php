<?php
require_once './dbm.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $maxIdQuery = 'SELECT MAX(id) FROM questions.users;';
    $stm = $dbm->prepare($maxIdQuery);
    $stm->execute();
    $numUsers = $stm->fetchAll();

    $userId = intval($numUsers[0]['MAX(id)']) + 1;

    $createUserQuery = 'INSERT INTO questions.users(id, firstname, surname, gender) VALUES (' . $userId . ', \'' . sanitise('firstname') . '\', \'' . sanitise('surname') . '\', \'' . sanitise('gender') . '\');';

    $insertQuestionsQuery = "";

    for ($i = 1; $i < count($_POST) - 2; $i++) {
        $insertQuestionsQuery .= 'INSERT INTO questions.answers(user_id, question, answer) VALUES (' . $userId .', ' . $i . ', \'' . sanitise('question-' . $i) . '\');';
    }

    //echo $createUserQuery;

    $stm = $dbm->prepare($createUserQuery);
    $stm->execute();

    $stm = $dbm->prepare($insertQuestionsQuery);
    $stm->execute();

    header("Location: thank-you.php");
} else {
    http_response_code(400);
    echo 'Invalid request';
}

function sanitise(string $varname): string {
    return filter_input(INPUT_POST, $varname, FILTER_SANITIZE_MAGIC_QUOTES);
}