<?php
$questionFile = file_get_contents('./questions.json');
$questions = json_decode($questionFile);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Questions</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./style.css" type="text/css">
</head>
<body>
<div id="wrapper" class="container">
    <ul>
        <li>F means false</li>
        <li>Sl means slightly true</li>
        <li>MT means mostly true</li>
        <li>VT means very true</li>
    </ul>
    <main>
        <form action="./submit.php" method="post">
            <fieldset>
                <div>
                    <label for="firstname-input">First name: </label>
                    <input type="text" name="firstname" id="firstname-input" required>
                    <br>
                    <label for="surname-input">Last name: </label>
                    <input type="text" name="surname" id="surname-input" required>
                    <br>
                    <label for="gender-select">Gender: </label>
                    <select id="gender-select" name="gender" required>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="anonymous">Prefer not to specify</option>
                    </select>
                </div>
            </fieldset>
            <br>
            <fieldset>
                <?php
                foreach ($questions as $iterator => $question) {
                ?>
                    <div>
                        <label><?= $question?></label><br>
                        <input checked type="radio" name="question-<?= $iterator + 1 ?>" id="radio-btn-<?= $iterator . '-0' ?>" value="1" required>
                        <label for="radio-btn-<?= $iterator . '-0' ?>">F</label>
                        <input type="radio" name="question-<?= $iterator + 1 ?>" id="radio-btn-<?= $iterator . '-1' ?>" value="2" required>
                        <label for="radio-btn-<?= $iterator . '-1' ?>">Sl</label>
                        <input type="radio" name="question-<?= $iterator + 1 ?>" id="radio-btn-<?= $iterator . '-2' ?>" value="3" required>
                        <label for="radio-btn-<?= $iterator . '-2' ?>">MT</label>
                        <input type="radio" name="question-<?= $iterator + 1 ?>" id="radio-btn-<?= $iterator . '-3' ?>" value="4" required>
                        <label for="radio-btn-<?= $iterator . '-3' ?>">VT</label>
                        <br>
                        <br>
                    </div>
                <?php
                }
                ?>
            </fieldset>
            <input type="submit" value="Submit">
        </form>
    </main>
</div>
</body>
</html>

