USE questions;

DROP TABLE IF EXISTS answers;
DROP TABLE IF EXISTS users;

CREATE TABLE users (
    id              INT UNSIGNED AUTO_INCREMENT,
    firstname       TINYTEXT,
    surname         TINYTEXT,
    gender          TINYTEXT,

    PRIMARY KEY (id)
);

CREATE TABLE answers (
    id              INT UNSIGNED AUTO_INCREMENT,
    user_id         INT UNSIGNED,
    question        SMALLINT UNSIGNED,
    answer          TINYINT UNSIGNED,

    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

ALTER TABLE users AUTO_INCREMENT = 1;
ALTER TABLE answers AUTO_INCREMENT = 1;